import { Hobby } from 'domain/hobby/Hobby';
import { HobbyTypeEnum } from 'domain/hobby/HobbyType.enum';
import { InMemoryHobbyRepository } from 'secondary/hobby/InMemoryHobby.repository';

describe('InMemoryHobbyRepository', () => {
  it('Should list', () => {
    expect(new InMemoryHobbyRepository().list()).resolves.toEqual([
      new Hobby('Sacrifier des chèvres', HobbyTypeEnum.INTELLECT),
    ]);
  });
  it('Should add', () => {
    expect(new InMemoryHobbyRepository().add(new Hobby('name', HobbyTypeEnum.INTELLECT))).resolves.toEqual(
      new Hobby('name', HobbyTypeEnum.INTELLECT),
    );
  });
});
