import { Hobby } from 'domain/hobby/Hobby';
import { HobbyTypeEnum } from 'domain/hobby/HobbyType.enum';

describe('Hobby', () => {
  it('Should build', () => {
    const hobby = new Hobby('name', HobbyTypeEnum.SPORT);
    expect(hobby.name).toBe('name');
    expect(hobby.type).toBe(HobbyTypeEnum.SPORT);
  });
  it('Should be sport', () => {
    expect(new Hobby('name', HobbyTypeEnum.SPORT).isSport()).toBe(true);
  });
  it('Should not be sport', () => {
    expect(new Hobby('name', HobbyTypeEnum.INTELLECT).isSport()).toBe(false);
  });
});
