import { renderHook } from '@testing-library/react-hooks';
import { useContextDependency } from 'primary/hooks/useContextDependency';
describe('UseContextDependency', () => {
  it('Should get context', () => {
    const { result } = renderHook(() => useContextDependency());
    expect(result.current.hobbyRepository).toBeDefined();
  });
});
