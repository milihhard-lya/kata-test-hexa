// ***********************************************************
// This example support/component.ts is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';

import { mount } from 'cypress/react';
import { SinonStub } from 'cypress/types/sinon';
import { ReactNode, ReactPortal } from 'react';
// Alternatively you can use CommonJS syntax:
// require('./commands')
import ReactDOM from 'react-dom';

// Augment the Cypress namespace to include type definitions for
// your custom command.
// Alternatively, can be defined in cypress/support/component.d.ts
// with a <reference path="./component" /> at the top of your spec.
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Cypress {
    interface Chainable {
      mount: typeof mount;
      interceptFeatureSwitch(name: string, isEnabled?: boolean): Chainable;
      interceptAllFeatureSwitches(): Chainable;
    }
  }
}

Cypress.Commands.add('mount', mount);
Cypress.Commands.add('interceptFeatureSwitch', (name: string, isEnabled = true) => {
  cy.intercept(`http://localhost:8080/api/feature-switches/${name}`, {
    name,
    enabled: isEnabled,
  }).as(`feature-${name}`);
});
Cypress.Commands.add('interceptAllFeatureSwitches', () => {
  cy.intercept(`http://localhost:8080/api/feature-switches/*`, {
    name: 'feature-switch',
    enabled: true,
  });
});
export const mockPortal = (idDocument: string) => {
  let stubbedCreatePortal: SinonStub;
  beforeEach(() => {
    cy.stub(document, 'getElementById')
      .withArgs(idDocument)
      .callsFake(() => document.createElement('div'));
    stubbedCreatePortal = cy.stub(ReactDOM, 'createPortal').callsFake((children: ReactNode) => children as ReactPortal);
  });
  return () => stubbedCreatePortal;
};
// Example use:
// cy.mount(<MyComponent />)
