import codeCoverage from '@cypress/code-coverage/task';
import { defineConfig } from 'cypress';

import webpackConfig from './webpack.config';

export default defineConfig({
  viewportWidth: 1920,
  viewportHeight: 1080,
  video: false,
  component: {
    devServer: {
      framework: 'react',
      bundler: 'webpack',
      webpackConfig,
    },
    setupNodeEvents(on, config) {
      codeCoverage(on, config);
      config.env.reactDevtools = true;
      return config;
    },

    specPattern: 'cypress/components/**/*.cy.{js,jsx,ts,tsx}',
  },
});
