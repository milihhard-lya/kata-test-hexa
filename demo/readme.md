# OSS PWA

project run with React and webpack

## Install

```sh
npm ci
```

> Project will automatically install if you install the root project

## Documentation

- [Heaxagonal architecture](documentation/hexa-archi.md)
- [Design: Atomic design](documentation/atomic-design.md)
- [Design: A-B\_\_E--M naming convention](documentation/abem.md)

## Usefull commands

Start the project

```sh
npm start
```

Build the application

```sh
npm run build
```

Build the application for development

```sh
npm run build:dev
```

Run unit test and composant test

```sh
npm run test
```

Run unit test and composant test

```sh
npm run test:watch
```

Lint the application

```sh
npm run lint
```

Lint the application and fix small bug

```sh
npm run lint:fix
```
