import { ContextDependency } from 'primary/contextDependency';
import { InMemoryHobbyRepository } from 'secondary/hobby/InMemoryHobby.repository';

export const getDefaultContextDependencyForApp = (): ContextDependency => {
  return {
    hobbyRepository: new InMemoryHobbyRepository(),
  };
};
