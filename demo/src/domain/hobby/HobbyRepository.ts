import { Hobby } from './Hobby';

export interface HobbyRepository {
  list(): Promise<Hobby[]>;
  add(hobby: Hobby): Promise<Hobby>;
}
