import { HobbyTypeEnum } from './HobbyType.enum';

export class Hobby {
  constructor(public readonly name: string, public readonly type: HobbyTypeEnum) {}

  isSport() {
    return this.type === HobbyTypeEnum.SPORT;
  }
}
