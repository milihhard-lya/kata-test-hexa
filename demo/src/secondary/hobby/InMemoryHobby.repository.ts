import { Hobby } from 'domain/hobby/Hobby';
import { HobbyRepository } from 'domain/hobby/HobbyRepository';
import { HobbyTypeEnum } from 'domain/hobby/HobbyType.enum';

export class InMemoryHobbyRepository implements HobbyRepository {
  private hobbies: Hobby[] = [new Hobby('Sacrifier des chèvres', HobbyTypeEnum.INTELLECT)];
  list(): Promise<Hobby[]> {
    return Promise.resolve(this.hobbies);
  }
  add(hobby: Hobby): Promise<Hobby> {
    this.hobbies = [...this.hobbies, hobby];
    return Promise.resolve(hobby);
  }
}
