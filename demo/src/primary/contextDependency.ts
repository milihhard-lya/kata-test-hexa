import { HobbyRepository } from 'domain/hobby/HobbyRepository';
import { createContext } from 'react';

import { getDefaultContextDependencyForApp } from '@/dependency';

export type ContextDependency = {
  hobbyRepository: HobbyRepository;
};
export const contextDependency = createContext(getDefaultContextDependencyForApp());
