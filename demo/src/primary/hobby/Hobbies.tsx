import { Hobby } from 'domain/hobby/Hobby';
import { useContextDependency } from 'primary/hooks/useContextDependency';
import { ReactElement, useEffect, useState } from 'react';

import { HobbyCreate } from './HobbyCreate';

export const Hobbies = (): ReactElement => {
  const { hobbyRepository } = useContextDependency();
  const [hobbies, setHobbies] = useState<Hobby[]>([]);
  useEffect(() => {
    hobbyRepository.list().then(setHobbies);
  }, []);

  return (
    <div className="app">
      <h1>Mes hobbies</h1>
      {hobbies.map((hobby, index) => (
        <div key={index}>
          <p>
            <span>{hobby.name}</span>
            <span> ({hobby.type})</span>
          </p>
        </div>
      ))}
      <HobbyCreate
        addHobby={(hobby) => {
          setHobbies((old) => [...old, hobby]);
        }}
      />
    </div>
  );
};
