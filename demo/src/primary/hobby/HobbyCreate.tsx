import { Hobby } from 'domain/hobby/Hobby';
import { HobbyTypeEnum } from 'domain/hobby/HobbyType.enum';
import { useContextDependency } from 'primary/hooks/useContextDependency';
import { ReactElement, useState } from 'react';
type HobbyCreateProps = {
  addHobby: (hobby: Hobby) => void;
};

export const HobbyCreate = ({ addHobby }: HobbyCreateProps): ReactElement => {
  const { hobbyRepository } = useContextDependency();
  const [name, setName] = useState('');
  return (
    <form
      onSubmit={async (e) => {
        e.preventDefault();
        const dataForm = new FormData(e.target as HTMLFormElement);
        const data: Record<string, string> = {};
        for (const [key, value] of dataForm.entries()) {
          data[key] = value as string;
        }
        if (!data.name || !data.type) {
          return;
        }
        const hobby = await hobbyRepository.add(new Hobby(data.name, data.type as HobbyTypeEnum));
        addHobby(hobby);
        setName('');
      }}
    >
      <input type="text" name="name" value={name} onChange={(e) => setName(e.target.value)} />
      <select name="type" id="type">
        {Object.entries(HobbyTypeEnum).map(([key, value]) => (
          <option key={key} value={key}>
            {value}
          </option>
        ))}
      </select>
      <button type="submit">new hobby</button>
    </form>
  );
};
