#!/usr/bin/env zx

$.verbose = false;

const splitFilenameFromFilepath = (filePath) => {
  const dirs = filePath.split('/');
  const filename = dirs.pop();
  return [dirs.join('/'), filename];
};

const createFileWithContent = async (baseDir, filePath, content) => {
  const [dirsWithoutFile] = splitFilenameFromFilepath(filePath);
  await $`mkdir -p ${dirsWithoutFile}`;
  await $`touch ${filePath}`;
  await fs.writeFile(filePath, content);
  console.log(chalk.green(`File ${chalk.bold(filePath.replace(baseDir, ''))} created`));
  $`code ${filePath}`;
};
const fileNameToClassname = (filename) => {
  return filename
    .split('.')
    .map((f) => f.charAt(0).toUpperCase() + f.substr(1))
    .join('');
};
const fileNameToLowerCawse = (filename) => {
  return filename
    .split('.')
    .map((f) => f.charAt(0).toLowerCase() + f.substr(1))
    .join('');
};
const buildNonPrimary = (filePath) => {
  const [, filename] = splitFilenameFromFilepath(filePath);
  return `export class ${fileNameToClassname(filename)} {
  constructor() {

  }
}
`;
};
const buildTest = (filePath, fileType) => {
  const [, filename] = splitFilenameFromFilepath(filePath);
  const classname = fileNameToClassname(filename);
  return `import { ${classname} } from '${fileType}/${filePath}';
describe('${classname}', () => {
  it('Should ', () => {
    //Given

    // When

    // Then
  });
});
`;
};
const buildPrimary = (filePath, withStyle = true) => {
  const [, filename] = splitFilenameFromFilepath(filePath);
  const classname = fileNameToClassname(filename);
  return `import { FC } from 'react';
${withStyle ? `import './${filename}.scss';` : ''}
interface ${classname}Props {

}

export const ${classname}: FC<${classname}Props> = ({ }) => {
  return null;
};
`;
};
const buildStyle = (filePath, fileType) => {
  const [, filename] = splitFilenameFromFilepath(filePath);
  const classname = fileNameToClassname(filename);
  return `.${fileType.charAt(0)}-${classname.charAt(0).toLowerCase() + classname.substr(1)} {

}
`;
};
const buildTestPrimary = (filePath) => {
  const [, filename] = splitFilenameFromFilepath(filePath);
  const classname = fileNameToClassname(filename);
  return `import { ${classname} } from 'primary/${filePath}';
import { render, screen, fireEvent } from '@testing-library/react';
import { RenderBuilder } from 'testUtils/renderBuilder';
describe('${classname}', () => {
  it('Should render', () => {
    //Given

    // When
    RenderBuilder.withUI(<${classname} />).render();

    // Then
  });
});
`;
};
const buildTestPrimaryAtomic = (filePath, fileType) => {
  const [, filename] = splitFilenameFromFilepath(filePath);
  const classname = fileNameToClassname(filename);
  return `import { ${classname} } from 'primary/${fileType}s/${filePath}';
import { render, screen, fireEvent } from '@testing-library/react';
import { RenderBuilder } from 'testUtils/renderBuilder';
describe('${classname}', () => {
  it('Should render', () => {
    //Given

    // When
    RenderBuilder.withUI(<${classname} />).render();

    // Then
  });
});
`;
};
const buildBuilderTest = (filePath, fileType) => {
  const [, filename] = splitFilenameFromFilepath(filePath);
  const classname = fileNameToClassname(filename);
  return `import { ${classname} } from '${fileType}/${filePath}';
export class ${classname}BuilderTest {
  build(): ${classname} {
    return new ${classname}();
  }
}
`;
};

const primaries = ['atom', 'molecule', 'organism', 'template', 'page'];
const [, , , fileType, filePath, baseDir] = process.argv;
let rightFilePath = filePath;
if (filePath.split('/').length === 1) {
  rightFilePath = `${fileNameToLowerCawse(filePath)}/${fileNameToClassname(filePath)}`;
}
const filePathWithFileType = `${rightFilePath}.${fileType}`;
if (primaries.includes(fileType)) {
  await createFileWithContent(
    baseDir,
    `${path.join(baseDir, 'src/primary', fileType + 's', `${filePathWithFileType}.tsx`)}`,
    buildPrimary(filePathWithFileType),
  );
  await createFileWithContent(
    baseDir,
    `${path.join(baseDir, 'src/primary', fileType + 's', `${filePathWithFileType}.scss`)}`,
    buildStyle(rightFilePath, fileType),
  );
  if (fileType !== 'template') {
    await createFileWithContent(
      baseDir,
      `${path.join(baseDir, 'test/primary', fileType + 's', `${filePathWithFileType}.spec.tsx`)}`,
      buildTestPrimaryAtomic(filePathWithFileType, fileType),
    );
  }
} else if (fileType === 'primary') {
  await createFileWithContent(
    baseDir,
    `${path.join(baseDir, 'src/primary', `${rightFilePath}.tsx`)}`,
    buildPrimary(rightFilePath, false),
  );
  await createFileWithContent(
    baseDir,
    `${path.join(baseDir, 'test/primary', `${rightFilePath}.spec.tsx`)}`,
    buildTestPrimary(rightFilePath),
  );
} else if (fileType === 'hook') {
  await createFileWithContent(
    baseDir,
    `${path.join(baseDir, 'src/primary', fileType + 's', `${filePathWithFileType}.ts`)}`,
    buildNonPrimary(rightFilePath),
  );
  await createFileWithContent(
    baseDir,
    `${path.join(baseDir, 'test/primary', fileType + 's', `${filePathWithFileType}.spec.ts`)}`,
    buildTest(rightFilePath, fileType),
  );
} else {
  await createFileWithContent(
    baseDir,
    `${path.join(baseDir, 'src', fileType, `${rightFilePath}.ts`)}`,
    buildNonPrimary(rightFilePath),
  );
  await createFileWithContent(
    baseDir,
    `${path.join(baseDir, 'test', fileType, `${rightFilePath}.spec.ts`)}`,
    buildTest(rightFilePath, fileType),
  );
  if (fileType === 'domain' && filePath.split('.').length === 1) {
    await createFileWithContent(
      baseDir,
      `${path.join(baseDir, 'test/testUtils/builder', `${rightFilePath}BuilderTest.ts`)}`,
      buildBuilderTest(rightFilePath, fileType),
    );
  }
}
