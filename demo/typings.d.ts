type SvgComponent = VFC<SVGProps<SVGSVGElement>>;

declare module '*.svg' {
  const SVG: SvgComponent;
  export default SVG;
}

declare module '*.png' {
  const value: string;
  export default value;
}
declare module '*.jpg' {
  const value: string;
  export default value;
}

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {
  interface Chainable {
    selectFirstOpt(label: string, textOption: string): Promise<void>;
  }
}
