import { Resource } from 'domain/resource/Resource';
import { ResourceTypeEnum } from 'domain/resource/ResourceType.enum';

describe('Resource', () => {
  it('Should build', () => {
    const resource = new Resource('resource', ResourceTypeEnum.IT);
    expect(resource.name).toBe('resource');
    expect(resource.type).toBe(ResourceTypeEnum.IT);
  });
  it('Should convert to string', () => {
    expect(new Resource('name', ResourceTypeEnum.OTHER).toString()).toBe('name (Autres)');
  });
});
