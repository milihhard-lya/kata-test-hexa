import { Resource } from 'domain/resource/Resource';
import { ResourceTypeEnum } from 'domain/resource/ResourceType.enum';
import { Zones } from 'domain/resource/Zones';

describe('Zones', () => {
  const resource1 = new Resource('computer', ResourceTypeEnum.IT);
  const resource2 = new Resource('chair', ResourceTypeEnum.FURNITURE);
  it('Should build', () => {
    const zones = new Zones([resource1], [resource2]);
    expect(zones.zone1).toEqual([resource1]);
    expect(zones.zone2).toEqual([resource2]);
  });
  it('Should add', () => {
    const zones = new Zones([resource1], [resource2]);
    zones.add('zone1', new Resource('name', ResourceTypeEnum.OTHER));
    expect(zones.zone1).toHaveLength(2);
    expect(zones.zone2).toHaveLength(1);
  });
  it('Should remove', () => {
    const zones = new Zones([resource1, resource2], [resource2]);
    zones.remove('zone1', resource1);
    expect(zones.zone1).toHaveLength(1);
    expect(zones.zone2).toHaveLength(1);
  });
});
