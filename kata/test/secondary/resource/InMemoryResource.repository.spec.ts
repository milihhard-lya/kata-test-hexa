import { Resource } from 'domain/resource/Resource';
import { ResourceTypeEnum } from 'domain/resource/ResourceType.enum';
import { Zones } from 'domain/resource/Zones';
import { InMemoryResourceRepository } from 'secondary/resource/InMemoryResource.repository';

describe('InMemoryResourceRepository', () => {
  const resource1 = new Resource('computer', ResourceTypeEnum.IT);
  const resource2 = new Resource('chair', ResourceTypeEnum.FURNITURE);
  const createZone = () => new Zones([resource1], [resource2]);

  it('Should list', async () => {
    const zones = await new InMemoryResourceRepository(createZone()).list();
    expect(zones.zone1).toHaveLength(1);
    expect(zones.zone2).toHaveLength(1);
  });
  it('Should switch to zone1', async () => {
    const repository = new InMemoryResourceRepository(createZone());
    await repository.switchResource(resource1, 'zone1');
    const zones = await repository.list();
    expect(zones.zone1).toHaveLength(0);
    expect(zones.zone2).toHaveLength(2);
  });
  it('Should switch to zone2', async () => {
    const repository = new InMemoryResourceRepository(createZone());
    await repository.switchResource(resource2, 'zone2');
    const zones = await repository.list();
    expect(zones.zone1).toHaveLength(2);
    expect(zones.zone2).toHaveLength(0);
  });
});
