/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const open = !!process.env.OPEN;
const isProduction = process.env.NODE_ENV === 'production';
const isCypress = process.env.CYPRESS === 'true';
const isTesting = process.env.TESTING_MODE === 'true';

let babelPlugins = [];
if (isTesting) {
  babelPlugins.push(require.resolve('@babel/plugin-proposal-class-properties'));
}
if (isCypress) {
  babelPlugins.push('istanbul');
}
const tsLoaderToUse = isTesting
  ? {
      loader: 'babel-loader',
      options: {
        // Babel must be required like this to be able to use npm-link to link to shared code, see:
        // https://stackoverflow.com/questions/34574403/how-to-set-resolve-for-babel-loader-presets/
        presets: [
          [
            '@babel/preset-env',
            {
              useBuiltIns: 'entry',
              corejs: {
                version: '3.20',
              },
            },
          ],
          ['@babel/preset-react', { runtime: 'automatic' }],
          '@babel/preset-typescript',
        ],
        plugins: babelPlugins,
      },
    }
  : 'ts-loader';
module.exports = {
  entry: path.join(__dirname, 'src', 'index.tsx'),
  output: { path: path.join(__dirname, 'dist'), filename: '[name].bundle.js', publicPath: '/' },
  mode: isProduction ? 'production' : 'development',
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: ['.tsx', '.ts', '.js'],
    alias: {
      domain: path.resolve(__dirname, 'src/domain/'),
      secondary: path.resolve(__dirname, 'src/secondary/'),
      primary: path.resolve(__dirname, 'src/primary/'),
      assets: path.resolve(__dirname, 'src/assets/'),
      MountBuilder: path.resolve(__dirname, 'cypress/support/MountBuilder'),
      testUtils: path.resolve(__dirname, 'test/testUtils/'),
      '@': path.resolve(__dirname, 'src/'),
    },
    fallback: {
      fs: false,
      os: require.resolve('os-browserify/browser'),
    },
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },
  devServer: {
    static: path.join(__dirname, 'src'),
    port: isCypress ? 8001 : 8100,
    historyApiFallback: true,
    open: open,
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: [/node_modules/, '/test'],
        use: [tsLoaderToUse],
      },
      {
        test: /\.(css)$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sassOptions: {
                includePaths: ['src/primary'],
              },
            },
          },
        ],
      },

      {
        test: /\.(jpg|jpeg|png|gif|mp3)$/,
        use: ['file-loader'],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'index.html'),
    }),
    new CopyPlugin({
      patterns: [{ from: 'public', to: '' }],
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
    new Dotenv({
      ignoreStub: true,
      systemvars: true,
    }),
  ],
};
