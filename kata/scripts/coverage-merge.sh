#!/bin/sh

mkdir -p reports/coverage-merge
cp reports/coverage-jest/coverage-final.json reports/coverage-merge/coverage-jest.json
cp reports/coverage-cypress/coverage-final.json reports/coverage-merge/coverage-cypress.json
npx nyc merge reports/coverage-merge ./.nyc_output/out.json
rm -rf reports/coverage-merge
