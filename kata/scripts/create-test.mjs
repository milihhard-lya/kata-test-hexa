#!/usr/bin/env zx

$.verbose = false;

const splitFilenameFromFilepath = (filePath) => {
  const dirs = filePath.split('/');
  const filename = dirs.pop();
  return [dirs.join('/'), filename];
};

const goToFile = async (fileName) => {
  await $`code ${fileName}`;
};

const getContentFile = (fileTestName) => {
  const fileName = fileTestName.split('/').pop();
  const name = fileName
    .split('.')
    .slice(0, -2)
    .map((part) => part.charAt(0).toUpperCase() + part.slice(1))
    .join('');
  return `describe('${name}', () => {
  it('Should', () => {

  });
});`;
};

const createTestFile = async (fileTestName, fileRelative) => {
  const [dirsWithoutFile] = splitFilenameFromFilepath(fileTestName);
  await $`mkdir -p ${dirsWithoutFile}`;
  await $`touch ${fileTestName}`;
  await fs.writeFileSync(fileTestName, getContentFile(fileTestName));
  console.log(chalk.green(`File ${chalk.bold(fileRelative)} created`));
  await goToFile(fileTestName);
};

const manageFile = async (fileTestName, baseDir) => {
  const fileRelative = fileTestName.replace(baseDir, '');
  if (fs.existsSync(fileTestName)) {
    console.log(chalk.green('Test file exist'));
    console.log(chalk.green(`Go to ${fileRelative}`));
    await goToFile(fileTestName);
  } else {
    console.log(chalk.green("Test file doesn't exist"));
    console.log(chalk.green(`Creating ${fileRelative} ...`));
    await createTestFile(fileTestName, fileRelative);
  }
};

const [, , , testType, file, baseDir] = process.argv;
let fileRelative = file.replace(`${baseDir}/src/`, '');
const extensionFile = fileRelative.split('.');
if (testType === 'unit') {
  extensionFile.splice(-1, 0, 'spec');
  fileRelative = extensionFile.join('.');
  const testName = `${baseDir}/test/${fileRelative}`;
  manageFile(testName, baseDir);
} else {
  extensionFile.splice(-1, 0, 'cy');
  fileRelative = extensionFile.join('.');
  const testName = `${baseDir}/cypress/components/${fileRelative.replace('primary/', '')}`;
  manageFile(testName, baseDir);
}
