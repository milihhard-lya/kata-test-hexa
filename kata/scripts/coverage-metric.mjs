#!/usr/bin/env zx

$.verbose = false;

const coverageTotal = await $`npx nyc report -r text-summary`;
const [, statement, branch, func, line] =
  /: (\d+\.?\d*)% \(.*: (\d+\.?\d*)% \(.*: (\d+\.?\d*)% \(.*: (\d+\.?\d*)% \(/.exec(
    coverageTotal.toString().replaceAll('\n', ''),
  );
console.log(statement, branch, func, line);
const coverageAvg = ((Number(statement) + Number(branch) + Number(func) + Number(line)) / 4).toFixed(2);
console.log(`Average coverage = ${coverageAvg}`);
$`echo ${coverageAvg} > output.txt`;
