import { Resource } from 'domain/resource/Resource';
import { ResourceTypeEnum } from 'domain/resource/ResourceType.enum';
import { Zones } from 'domain/resource/Zones';
import { ContextDependency } from 'primary/contextDependency';
import { InMemoryResourceRepository } from 'secondary/resource/InMemoryResource.repository';

export const getDefaultContextDependencyForApp = (): ContextDependency => {
  return {
    resourceRepository: new InMemoryResourceRepository(
      new Zones(
        [
          new Resource('Table', ResourceTypeEnum.FURNITURE),
          new Resource('Chaise', ResourceTypeEnum.FURNITURE),
          new Resource('PC portable', ResourceTypeEnum.IT),
        ],
        [
          new Resource('Le COVID de Charles', ResourceTypeEnum.OTHER),
          new Resource("L'EPID de PA", ResourceTypeEnum.OTHER),
          new Resource('La charisme de Wael', ResourceTypeEnum.OTHER),
          new Resource('La créativité limite de Emilien', ResourceTypeEnum.OTHER),
        ],
      ),
    ),
  };
};
