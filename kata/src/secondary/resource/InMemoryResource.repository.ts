import { Resource } from 'domain/resource/Resource';
import { ResourceRepository } from 'domain/resource/ResourceRepository';
import { Zones, ZoneType } from 'domain/resource/Zones';

export class InMemoryResourceRepository implements ResourceRepository {
  constructor(private zones: Zones) {}

  list(): Promise<Zones> {
    return Promise.resolve(this.zones);
  }
  async switchResource(resource: Resource, zone: ZoneType): Promise<void> {
    if (zone === 'zone1') {
      this.zones.remove('zone1', resource);
      this.zones.add('zone2', resource);
    } else {
      this.zones.remove('zone2', resource);
      this.zones.add('zone1', resource);
    }
    this.zones = new Zones([...this.zones.zone1], [...this.zones.zone2]);
  }
}
