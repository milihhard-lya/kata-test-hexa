import { ContextDependency, contextDependency } from 'primary/contextDependency';
import { useContext } from 'react';

/**
 * Hook for consuming context dependency
 * @returns {ContextDependency} ContextDependency
 */
export const useContextDependency = (): ContextDependency => useContext(contextDependency);
