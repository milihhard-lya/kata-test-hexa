import './ZonesView.scss';

import { Zones } from 'domain/resource/Zones';
import { useContextDependency } from 'primary/hooks/useContextDependency';
import { ReactElement, useEffect, useState } from 'react';

import { ResourceItem } from './item/ResourceItem';
export const ZonesView = (): ReactElement => {
  const { resourceRepository } = useContextDependency();
  const [zones, setZones] = useState<Zones>(new Zones([], []));
  async function listZones() {
    const zones = await resourceRepository.list();
    setZones(zones);
  }
  useEffect(() => {
    listZones();
  }, []);

  return (
    <div>
      <h1>Inventaire</h1>
      <div className="zones">
        <div>
          <h2>Zone 1</h2>
          {zones.zone1.map((resource, index) => (
            <ResourceItem key={index} resource={resource} zone="zone1" updateZones={listZones} />
          ))}
        </div>
        <div>
          <h2>Zone 2</h2>
          {zones.zone2.map((resource, index) => (
            <ResourceItem key={index} resource={resource} zone="zone2" updateZones={listZones} />
          ))}
        </div>
      </div>
    </div>
  );
};
