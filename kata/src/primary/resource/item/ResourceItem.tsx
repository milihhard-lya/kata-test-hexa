import './ResourceItem.scss';

import classnames from 'classnames';
import { Resource } from 'domain/resource/Resource';
import { ZoneType } from 'domain/resource/Zones';
import { useContextDependency } from 'primary/hooks/useContextDependency';
import { ReactElement } from 'react';
interface ResourceItemProps {
  resource: Resource;
  zone: ZoneType;
  updateZones: () => void;
}

export const ResourceItem = ({ zone, resource, updateZones }: ResourceItemProps): ReactElement => {
  const { resourceRepository } = useContextDependency();
  return (
    <div
      onClick={async () => {
        await resourceRepository.switchResource(resource, zone);
        updateZones();
      }}
      className={classnames('resource', `-${zone}`)}
    >
      <p>{resource.toString()}</p>
    </div>
  );
};
