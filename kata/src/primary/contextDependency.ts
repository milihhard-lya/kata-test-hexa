import { ResourceRepository } from 'domain/resource/ResourceRepository';
import { createContext } from 'react';

import { getDefaultContextDependencyForApp } from '@/dependency';

export type ContextDependency = {
  resourceRepository: ResourceRepository;
};
export const contextDependency = createContext(getDefaultContextDependencyForApp());
