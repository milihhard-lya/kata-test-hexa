import './App.scss';

import { ZonesView } from './resource/ZonesView';
export const App = () => (
  <div className="app">
    <ZonesView />
  </div>
);
