export enum ResourceTypeEnum {
  IT = 'Informatique',
  HARDWARE = 'Hardware',
  FURNITURE = 'Meubles',
  OTHER = 'Autres',
}
