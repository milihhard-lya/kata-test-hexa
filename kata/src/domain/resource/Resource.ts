import { ResourceTypeEnum } from './ResourceType.enum';

export class Resource {
  constructor(public readonly name: string, public readonly type: ResourceTypeEnum) {}

  toString(): string {
    return `${this.name} (${this.type})`;
  }
}
