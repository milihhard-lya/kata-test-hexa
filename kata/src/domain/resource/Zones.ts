import { Resource } from './Resource';

export type ZoneType = 'zone1' | 'zone2';

export class Zones {
  private readonly zones: Record<ZoneType, Resource[]>;
  constructor(zone1: Resource[], zone2: Resource[]) {
    this.zones = {
      zone1,
      zone2,
    };
  }
  remove(zone: ZoneType, resource: Resource) {
    return this.zones[zone].splice(this.zones[zone].indexOf(resource), 1);
  }
  add(zone: ZoneType, resource: Resource) {
    this.zones[zone].push(resource);
  }
  get zone1() {
    return this.zones.zone1;
  }
  get zone2() {
    return this.zones.zone2;
  }
}
