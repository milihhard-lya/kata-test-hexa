import { Resource } from './Resource';
import { Zones, ZoneType } from './Zones';

export interface ResourceRepository {
  list(): Promise<Zones>;
  switchResource(resource: Resource, zone: ZoneType): Promise<void>;
}
