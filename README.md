# Kata : Frontend test on hexagonal architecture

The aim of the kata is to view au to test a frontend application with an hexagonal architecture.

## 2 Steps
- Explanation: `demo` dir
- Exercise: `kata` dir

## Explanation
the `demo` dir contains a project with a code tested. It can be use a an example on how to use test in frontend application.

## Kata
the `kata` dir contains the exercise. It's a functional application without tests. 
The aim is to have a 100% coverage on this application

> The application can be changed to simplify the tests